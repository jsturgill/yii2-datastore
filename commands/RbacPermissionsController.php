<?php
namespace JSturgill\Yii2\Datastore\commands;

use Yii;
use yii\console\Controller;

/**
 * Configures permissions and roles for the Datastore extension
 *
 */
class RbacPermissionsController extends Controller
{
    /**
     * Create Datastore extension permissions (create, update, read) and roles (user, admin)
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // add "createPerson" permission
        $createPerson = $auth->createPermission('datastore.createPerson');
        $createPerson->description = 'Create a Person';
        $auth->add($createPerson);

        // add "updatePerson" permission
        $updatePerson = $auth->createPermission('datastore.updatePerson');
        $updatePerson->description = 'Update Person';
        $auth->add($updatePerson);

        // add "readPerson" permission
        $readPerson = $auth->createPermission('datastore.readPerson');
        $readPerson->description = 'Read Person';
        $auth->add($readPerson);

        // add "datastoreReader" role and give this role the "readPerson" permission
        $datastoreReader = $auth->createRole('datastore.reader');
        $auth->add($datastoreReader);
        $auth->addChild($datastoreReader, $readPerson);

        // add "datastoreContributor" role and give this role the "createPerson" permission
        // as well as the permissions of the "datastoreReader" role
        $datastoreContributor = $auth->createRole('datastore.contributor');
        $auth->add($datastoreContributor);
        $auth->addChild($datastoreContributor, $createPerson);
        $auth->addChild($datastoreContributor, $datastoreReader);

        // add "datastoreAdmin" role and give this role the "updatePerson" permission
        // as well as the permissions of the "datastoreContributor" role
        $datastoreAdmin = $auth->createRole('datastore.admin');
        $auth->add($datastoreAdmin);
        $auth->addChild($datastoreAdmin, $updatePerson);
        $auth->addChild($datastoreAdmin, $datastoreContributor);
    }
}
