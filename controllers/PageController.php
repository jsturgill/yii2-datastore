<?php
namespace JSturgill\Yii2\Datastore\controllers;

use yii\web\Controller;

class PageController extends Controller
{
    /**
     * @var string
     */
    public $layout = '@app/views/layouts/main';

    /**
     * @var mixed
     */
    public $module;

    /**
     * @param string $view
     * @return mixed
     */
    public function actionView($view = 'index.php')
    {
        $view = ('' === $view) ? 'index' : $view;
        return $this->render($view);
    }
}
