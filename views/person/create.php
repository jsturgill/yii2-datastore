<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model JSturgill\Yii2\Datastore\models\Person */

$this->title = 'Create Person';
$this->params['breadcrumbs'][] = Yii::$app->controller->module->id;
$this->params['breadcrumbs'][] = ['label' => 'Person', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
