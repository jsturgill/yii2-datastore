<?php

use yii\db\Migration;

class m160308_232612_add_created_at_and_updated_at_to_persons extends Migration
{
    public function safeUp()
    {
        $this->renameTable('persons_table', 'persons');
        $this->addColumn('persons', 'created_at', $this->datetime());
        $this->addColumn('persons', 'updated_at', $this->datetime());
    }

    public function safeDown()
    {
        $this->renameTable('persons', 'persons_table');
        $this->dropColumn('persons', 'created_at');
        $this->dropColumn('persons', 'updated_at');
    }
}
