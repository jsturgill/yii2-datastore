<?php

use yii\db\Migration;

class m160308_201121_create_persons_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('persons_table', [
            'id'         => $this->primaryKey(),
            'firstname'  => $this->string()->notNull(),
            'lastname'   => $this->string()->notNull(),
            'birthyear'  => $this->smallinteger(4),
            'birthmonth' => $this->smallinteger(2),
            'birthday'   => $this->smallinteger(2),
            'zipcode'    => $this->smallinteger(9),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('persons_table');
    }
}
