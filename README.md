# Datastore Installation

## Prerequisites

Your Yii 2 application must have RBAC set up. Run the RBAC migration:

`php yii migrate --migrationPath=@yii/rbac/migrations`

and ensure the `authManager` and `user` components are configured.

## Require

```
#!javascript
{
    //...
    "require": {
        //...
        "jsturgill/yii2-datastore": "dev-test"
    },
    "repositories": [
        //...
        {
            "type": "git",
            "url":  "git@bitbucket.org:jsturgill/yii2-datastore.git"
        }
    ],
    //...
}
```
Run `composer update`.

## Configure

```
#!php
<?php
// @app/config/web.php and @app/config/console.php
// (both are necessary)
return [
    'bootstrap'  => ['datastore'],
    'modules'             => [
        'datastore' => [
            'class' => 'JSturgill\Yii2\Datastore\Module',
        ],
    ],
];
```

## Migrate

```
#!bash

# assumes the extension was installed via composer and placed in @app/vendor/jsturgill/yii2-datastore
php yii migrate/up --migrationPath=@vendor/jsturgill/yii2-datastore/migrations
```


# Controlling Access

Datastore readers (no write access) should have the following role: `datastoreReader`.

Datastore contributors (read and insert access) should have the following role: `datastoreContributor`.

Datastore admins should have the following role: `datastoreAdmin`.