<?php
/**
 * @link https://bitbucket.org/jsturgill/yii2-datastore
 * @copyright Copyright (c) 2016 J Sturgill Designs
 */
namespace JSturgill\Yii2\Datastore;

use Yii;
use yii\base\BootstrapInterface;

/**
 *
 * @author Jeremiah Sturgill <jsturgill@gmail.com>
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    const CONSOLE_CONTROLLER_NAMESPACE = 'JSturgill\Yii2\Datastore\commands';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'JSturgill\Yii2\Datastore\controllers';

    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {

        if ($app instanceof \yii\web\Application) {
            $app->getUrlManager()->addRules([
                $this->id . '/page/<view:.*>' => $this->id . '/page/view',
            ], false);
        } else {
            $app->controllerMap[$this->id] = [
                'class'  => self::CONSOLE_CONTROLLER_NAMESPACE . '\RbacPermissionsController',
                'module' => $this,
            ];
        }

    }

    public function init()
    {
        parent::init();

        if (Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = self::CONSOLE_CONTROLLER_NAMESPACE;
        }

    }

}
