<?php

namespace JSturgill\Yii2\Datastore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use JSturgill\Yii2\Datastore\models\Person;

/**
 * PersonSearch represents the model behind the search form about `JSturgill\Yii2\Datastore\models\Person`.
 */
class PersonSearch extends Person
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'birthyear', 'birthmonth', 'birthday', 'zipcode'], 'integer'],
            [['firstname', 'lastname', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Person::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'birthyear' => $this->birthyear,
            'birthmonth' => $this->birthmonth,
            'birthday' => $this->birthday,
            'zipcode' => $this->zipcode,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname]);

        return $dataProvider;
    }
}
