<?php

namespace JSturgill\Yii2\Datastore\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "persons".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property integer $birthyear
 * @property integer $birthmonth
 * @property integer $birthday
 * @property integer $zipcode
 * @property string $created_at
 * @property string $updated_at
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'firstname'  => 'Firstname',
            'lastname'   => 'Lastname',
            'birthyear'  => 'Birthyear',
            'birthmonth' => 'Birthmonth',
            'birthday'   => 'Birthday',
            'zipcode'    => 'Zipcode',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'class'      => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
            ],
            'value'      => new Expression('NOW()'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'birthyear', 'birthmonth', 'birthday', 'zipcode'], 'trim'],
            [['firstname', 'lastname', 'birthyear', 'birthmonth', 'birthday', 'zipcode'], 'required'],
            [['firstname', 'lastname'], 'string', 'min' => 1, 'max' => 256],
            ['birthyear', 'integer'],
            ['birthmonth', 'integer', 'min' => 1, 'max' => 12],
            ['birthday', 'integer', 'min' => 1, 'max' => 31],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persons';
    }
}
